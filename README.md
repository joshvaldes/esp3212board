![cc88x31.png](https://bitbucket.org/repo/LGjnpp/images/822030218-cc88x31.png)

Creative Commons License
[quickNdirty ESP32 board](https://bitbucket.org/joshvaldes/esp3212board) by [Josh Valdes](https://www.linkedin.com/in/joshvaldes) is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

[More info on Hackaday.io](https://hackaday.io/project/14001-quick-n-dirty-esp32-board)